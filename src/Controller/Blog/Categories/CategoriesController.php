<?php

namespace App\Controller\Blog\Categories;

use App\Model\Blog\Entity\Category\Category;
use App\ReadModel\Blog\Category\CategoryFetcher;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Blog\UseCase\Category\Create;
use App\Model\Blog\UseCase\Category\Edit;
use App\Model\Blog\UseCase\Category\Remove;
use App\Model\Blog\UseCase\Category\Status;
use App\Annotation\Guid;

/**
 * Class CategoriesController
 * @package App\Controller\Blog\Categories
 * @Route("/blog/categories", name="blog.categories.")
 */
class CategoriesController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CategoryFetcher
     */
    private $categoryFetcher;

    /**
     * CategoriesController constructor.
     * @param LoggerInterface $logger
     * @param CategoryFetcher $categoryFetcher
     */
    public function __construct(LoggerInterface $logger, CategoryFetcher $categoryFetcher)
    {
        $this->logger = $logger;
        $this->categoryFetcher = $categoryFetcher;
    }

    /**
     * @Route("/", name="index")
     * @return Response
     */
    public function index(): Response
    {
        $categories = $this->categoryFetcher->all();
        return $this->render('app/blog/categories/index.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @param Create\Handler $handler
     * @return Response
     */
    public function create(Request $request, Create\Handler $handler): Response
    {
        $command = new Create\Command();

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('blog.categories.index');
            } catch (\DomainException $e) {
                $this->logger->error($e->getMessage(), ['exception' => $e]);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/blog/categories/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit")
     * @param Category $category
     * @param Request $request
     * @param Edit\Handler $handler
     * @return Response
     */
    public function edit(Category $category, Request $request, Edit\Handler $handler): Response
    {
        $command = new Edit\Command($category);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('blog.categories.index');
            } catch (\DomainException $e) {
                $this->logger->error($e->getMessage(), ['exception' => $e]);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/blog/categories/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete", methods={"POST"})
     * @param Category $category
     * @param Remove\Handler $handler
     * @return Response
     */
    public function remove(Category $category, Remove\Handler $handler): Response
    {
        $command = new Remove\Command($category);
        try {
            $handler->handle($command);
        } catch (\DomainException $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
            $this->addFlash('error', $e->getMessage());
        }
        return $this->redirectToRoute('blog.categories.index');
    }

    /**
     * @Route("/{id}/status", name="status")
     * @param Category $category
     * @param Request $request
     * @param Status\Handler $handler
     * @return Response
     */
    public function changeStatus(Category $category, Request $request, Status\Handler $handler): Response
    {
        $command = new Status\Command($category);

        $form = $this->createForm(Status\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('blog.categories.index');
            } catch (\DomainException $e) {
                $this->logger->error($e->getMessage(), ['exception' => $e]);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/blog/categories/status.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", requirements={"id"=Guid::PATTERN})
     * @param Category $category
     * @return Response
     */
    public function show(Category $category): Response
    {
        return $this->render('app/blog/posts/view.html.twig', [
            'category' => $category,
        ]);
    }
}
