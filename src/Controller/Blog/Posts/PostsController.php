<?php
declare(strict_types=1);

namespace App\Controller\Blog\Posts;

use App\Model\Blog\Entity\Post\Post;
use App\ReadModel\Blog\Post\PostFetcher;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Model\Blog\UseCase\Post\Create;
use App\Model\Blog\UseCase\Post\Edit;
use App\Model\Blog\UseCase\Post\Remove;
use App\Model\Blog\UseCase\Post\Status;

/**
 * Class PostsController
 * @package App\Controller\Blog\Posts
 * @Route("/blog/posts", name="blog.posts.")
 */
class PostsController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PostFetcher
     */
    private $postsFetcher;

    /**
     * PostsController constructor.
     * @param LoggerInterface $logger
     * @param PostFetcher $postsFetcher
     */
    public function __construct(LoggerInterface $logger, PostFetcher $postsFetcher)
    {
        $this->logger = $logger;
        $this->postsFetcher = $postsFetcher;
    }

    /**
     * @Route("/", name="index")
     * @return Response
     */
    public function index(): Response
    {
        $posts = $this->postsFetcher->all();
        return $this->render('app/blog/posts/index.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @param Create\Handler $handler
     * @return Response
     */
    public function create(Request $request, Create\Handler $handler): Response
    {
        $author = "9373d725-1fb6-4d7e-9847-452eae8207e6";
        $command = new Create\Command($author);

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('blog.posts.index');
            } catch (\DomainException $e) {
                $this->logger->error($e->getMessage(), ['exception' => $e]);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/blog/posts/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit")
     * @param Post $post
     * @param Request $request
     * @param Edit\Handler $handler
     * @return Response
     */
    public function edit(Post $post, Request $request, Edit\Handler $handler): Response
    {
        $command = new Edit\Command($post);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('blog.posts.index');
            } catch (\DomainException $e) {
                $this->logger->error($e->getMessage(), ['exception' => $e]);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/blog/posts/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete", methods={"POST"})
     * @param Post $post
     * @param Remove\Handler $handler
     * @return Response
     */
    public function remove(Post $post, Remove\Handler $handler): Response
    {
        $command = new Remove\Command($post);
        try {
            $handler->handle($command);
        } catch (\DomainException $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
            $this->addFlash('error', $e->getMessage());
        }
        return $this->redirectToRoute('blog.posts.index');
    }

    /**
     * @Route("/{id}/status", name="status")
     * @param Post $post
     * @param Request $request
     * @param Status\Handler $handler
     * @return Response
     */
    public function changeStatus(Post $post, Request $request, Status\Handler $handler): Response
    {
        $command = new Status\Command($post);

        $form = $this->createForm(Status\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                return $this->redirectToRoute('blog.posts.index');
            } catch (\DomainException $e) {
                $this->logger->error($e->getMessage(), ['exception' => $e]);
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('app/blog/posts/status.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show")
     * @param Post $post
     * @return Response
     */
    public function show(Post $post): Response
    {
        return $this->render('app/blog/posts/view.html.twig', [
            'post' => $post,
        ]);
    }
}
