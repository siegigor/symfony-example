<?php
declare(strict_types=1);

namespace App\ReadModel\Blog\Category;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;

/**
 * Class CategoryFetcher
 * @package App\ReadModel\Blog\Category
 */
class CategoryFetcher
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * CategoryFetcher constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return array
     */
    public function assoc(): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'id',
                'title_title as title'
            )
            ->from('blog_categories')
            ->orderBy('title')
            ->execute();

        return $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    /**
     * @return array
     */
    public function all(): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'cat.id',
                'cat.status',
                'cat.title_title as title',
                'cat.title_slug as slug',
                'par.title_title as parent_title',
                '(SELECT COUNT(*) FROM blog_posts_categories p WHERE p.category_id = cat.id) AS posts'
            )
            ->from('blog_categories', 'cat')
            ->leftJoin('cat', 'blog_categories', 'par', 'cat.parent_id = par.id')
            ->orderBy('title')
            ->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }
}
