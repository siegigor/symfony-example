<?php
declare(strict_types=1);

namespace App\ReadModel\Blog\Post;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;

/**
 * Class PostFetcher
 * @package App\ReadModel\Blog\Post
 */
class PostFetcher
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * PostFetcher constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'posts.*',
                'CONCAT(author.name_first, \' \', author.name_last) as author_name'
            )
            ->from('blog_posts', 'posts')
            ->leftJoin('posts', 'blog_authors', 'author', 'posts.author_id = author.id')
            ->orderBy('posts.date')
            ->execute();

        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }
}
