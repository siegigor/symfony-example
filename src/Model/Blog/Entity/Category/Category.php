<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Category;

use App\Model\Blog\Entity\Category\ValueObject\Id;
use App\Model\Blog\Entity\Category\ValueObject\Status;
use App\Model\Blog\Entity\Category\ValueObject\Title;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Author
 * @package App\Model\Blog\Entity\Author
 * @ORM\Entity
 * @ORM\Table(name="blog_categories")
 */
class Category
{
    /**
     * @var Id
     * @ORM\Column(type="blog_category_id")
     * @ORM\Id
     */
    private $id;

    /**
     * @var Title
     * @ORM\Embedded(class="\App\Model\Blog\Entity\Category\ValueObject\Title")
     */
    private $title;

    /**
     * @var Status
     * @ORM\Column(type="blog_category_status", length=16)
     */
    private $status;

    /**
     * @var Category|null
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $parent;

    /**
     * Category constructor.
     * @param Id $id
     * @param Title $title
     */
    public function __construct(Id $id, Title $title)
    {
        $this->id = $id;
        $this->title = $title;
        $this->status = Status::active();
    }

    public function edit(Title $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Title
     */
    public function getTitle(): Title
    {
        return $this->title;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return Category|null
     */
    public function getParent(): ?Category
    {
        return $this->parent;
    }

    public function changeStatus(Status $status): void
    {
        if ($this->status->isEqual($status)) {
            throw new \DomainException("Status already is " . $status->getName());
        }
        $this->status = $status;
    }

    public function setAsRoot(): void
    {
        $this->parent = null;
    }

    public function setAsChildOf(self $parent): void
    {
        if ($parent === $this->parent) {
            return;
        }
        $current = $parent;
        do {
            if ($current === $this) {
                throw new \DomainException('Cyclic inheritance.');
            }
        }
        while ($current && $current = $current->getParent());
        $this->parent = $parent;
    }
}
