<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Category;

use App\Model\Blog\Entity\Category\ValueObject\Id;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CategoryRepository
 * @package App\Model\Blog\Entity\Category
 */
class CategoryRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CategoryRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository(Category::class);
        $this->em = $em;
    }

    public function get(Id $id): Category
    {
        /** @var Category $category */
        if (!$category = $this->repository->find($id->getValue())) {
            throw new \DomainException('Category not found.');
        }
        return $category;
    }

    /**
     * @param Id $id
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function hasChild(Id $id): bool
    {
        return $this->repository->createQueryBuilder('cat')
                ->select('COUNT(cat.id)')
                ->andWhere('cat.parent = :parent')
                ->setParameter(':parent', $id->getValue())
                ->getQuery()->getSingleScalarResult() > 0;
    }

    /**
     * @param Category $category
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function hasBySlug(Category $category): bool
    {
        return $this->repository->createQueryBuilder('cat')
                ->select('COUNT(cat.id)')
                ->andWhere('cat.title.slug = :slug')
                ->setParameter(':slug', $category->getTitle()->getSlug())
                ->andWhere('cat.id != :id')
                ->setParameter(':id', $category->getId()->getValue())
                ->getQuery()->getSingleScalarResult() > 0;
    }

    public function add(Category $category): void
    {
        $this->em->persist($category);
    }

    public function remove(Category $category): void
    {
        $this->em->remove($category);
    }
}
