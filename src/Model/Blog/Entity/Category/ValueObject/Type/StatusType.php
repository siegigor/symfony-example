<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Category\ValueObject\Type;

use App\Model\Blog\Entity\Category\ValueObject\Status;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

/**
 * Class StatusType
 * @package App\Model\Blog\Entity\Category\ValueObject\Type
 */
class StatusType extends StringType
{
    public const NAME = 'blog_category_status';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof Status ? $value->getName() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new Status($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
