<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Post\ValueObject\Type;

use App\Model\Blog\Entity\Post\ValueObject\Status;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

/**
 * Class StatusType
 * @package App\Model\Blog\Entity\Post\ValueObject\Type
 */
class StatusType extends StringType
{
    public const NAME = 'blog_post_status';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof Status ? $value->getName() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new Status($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
