<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Post\ValueObject\Type;

use App\Model\Blog\Entity\Post\ValueObject\Id;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

/**
 * Class IdType
 * @package App\Model\Blog\Entity\Post\ValueObject\Type
 */
class IdType extends IntegerType
{
    public const NAME = 'blog_post_id';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof Id ? $value->getValue() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new Id($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) : bool
    {
        return true;
    }
}
