<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Post\ValueObject;

use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Title
 * @package App\Model\Blog\Entity\Post\ValueObject
 * @ORM\Embeddable
 */
class Title
{
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * Title constructor.
     * @param string $title
     */
    public function __construct(string $title)
    {
        Assert::notEmpty($title);
        $this->title = $title;
        $this->setSlug();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    private function setSlug(): void
    {
        $this->slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $this->title)));
    }
}
