<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Post\ValueObject;

use Webmozart\Assert\Assert;

/**
 * Class Status
 * @package App\Model\Blog\Entity\Post\ValueObject
 */
class Status
{
    public const ACTIVE = 'active';
    public const NOT_ACTIVE = 'not active';

    private $name;

    /**
     * Status constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        Assert::oneOf($name, [
            self::ACTIVE,
            self::NOT_ACTIVE,
        ]);

        $this->name = $name;
    }

    public static function active(): self
    {
        return new self(self::ACTIVE);
    }

    public static function notActive(): self
    {
        return new self(self::NOT_ACTIVE);
    }

    public function isEqual(self $other): bool
    {
        return $this->getName() === $other->getName();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isActive(): bool
    {
        return $this->name === self::ACTIVE;
    }
}
