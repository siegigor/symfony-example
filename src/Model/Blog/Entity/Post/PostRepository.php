<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Post;

use App\Model\Blog\Entity\Post\ValueObject\Id;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PostRepository
 * @package App\Model\Blog\Entity\Post
 */
class PostRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $connection;

    /**
     * CategoryRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository(Post::class);
        $this->em = $em;
        $this->connection = $em->getConnection();
    }

    /**
     * @param Id $id
     * @return Post
     */
    public function get(Id $id): Post
    {
        /** @var Post $post */
        if (!$post = $this->repository->find($id->getValue())) {
            throw new \DomainException('Post not found.');
        }
        return $post;
    }

    /**
     * @return Id
     * @throws \Doctrine\DBAL\DBALException
     */
    public function nextId(): Id
    {
        return new Id((int)$this->connection->query('SELECT nextval(\'blog_posts_seq\')')->fetchColumn());
    }

    /**
     * @param Post $post
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function hasBySlug(Post $post): bool
    {
        return $this->repository->createQueryBuilder('p')
                ->select('COUNT(p.id)')
                ->andWhere('p.title.slug = :slug')
                ->setParameter(':slug', $post->getTitle()->getSlug())
                ->andWhere('p.id != :id')
                ->setParameter(':id', $post->getId()->getValue())
                ->getQuery()->getSingleScalarResult() > 0;
    }

    public function add(Post $post): void
    {
        $this->em->persist($post);
    }

    public function remove(Post $post): void
    {
        $this->em->remove($post);
    }
}