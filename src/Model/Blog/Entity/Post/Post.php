<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Post;

use App\Model\Blog\Entity\Author\Author;
use App\Model\Blog\Entity\Category\Category;
use App\Model\Blog\Entity\Post\ValueObject\Id;
use App\Model\Blog\Entity\Post\ValueObject\Status;
use App\Model\Blog\Entity\Post\ValueObject\Title;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 * @package App\Model\Blog\Entity\Post
 * @ORM\Entity
 * @ORM\Table(name="blog_posts", indexes={
 *     @ORM\Index(columns={"date"})
 * })
 */
class Post
{
    /**
     * @var Id
     * @ORM\Column(type="blog_post_id")
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="blog_posts_seq", initialValue=1)
     * @ORM\Id
     */
    private $id;

    /**
     * @var Title
     * @ORM\Embedded(class="\App\Model\Blog\Entity\Post\ValueObject\Title")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @var Status
     * @ORM\Column(type="blog_post_status", length=16)
     */
    private $status;

    /**
     * @var Author
     * @ORM\ManyToOne(targetEntity="\App\Model\Blog\Entity\Author\Author")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=false)
     */
    private $author;

    /**
     * @var Category[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Model\Blog\Entity\Category\Category")
     * @ORM\JoinTable(name="blog_posts_categories",
     *      joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"title.title" = "ASC"})
     */
    private $categories;

    /**
     * Post constructor.
     * @param Id $id
     * @param Title $title
     * @param Author $author
     * @param string $content
     * @param \DateTimeImmutable $date
     */
    public function __construct(Id $id, Title $title, Author $author, string $content, \DateTimeImmutable $date)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->author = $author;
        $this->date  = $date;
        $this->status = Status::active();
        $this->categories = new ArrayCollection();
    }

    /**
     * @param Title $title
     * @param string $content
     */
    public function edit(Title $title, string $content): void
    {
        $this->title = $title;
        $this->content = $content;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Title
     */
    public function getTitle(): Title
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return Author
     */
    public function getAuthor(): Author
    {
        return $this->author;
    }

    /**
     * @return Category[]|ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category $category
     */
    public function assignCategory(Category $category): void
    {
        if ($this->categories->contains($category)) {
            throw new \DomainException('Category is already assigned.');
        }
        $this->categories->add($category);
    }

    /**
     * @param Category $category
     */
    public function revokeCategory(Category $category): void
    {
        if (!$this->categories->contains($category)) {
            throw new \DomainException('Category is not assigned.');
        }
        $this->categories->removeElement($category);
    }

    public function changeStatus(Status $status): void
    {
        if ($this->status->isEqual($status)) {
            throw new \DomainException("Status already is " . $status->getName());
        }
        $this->status = $status;
    }
}
