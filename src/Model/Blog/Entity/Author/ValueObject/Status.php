<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Author\ValueObject;

use Webmozart\Assert\Assert;

/**
 * Class Status
 * @package App\Model\Blog\Entity\Author\ValueObject
 */
class Status
{
    public const ACTIVE = 'active';
    public const ARCHIVED = 'blocked';

    private $name;

    /**
     * Status constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        Assert::oneOf($name, [
            self::ACTIVE,
            self::ARCHIVED,
        ]);

        $this->name = $name;
    }

    public static function active(): self
    {
        return new self(self::ACTIVE);
    }

    public static function blocked(): self
    {
        return new self(self::ARCHIVED);
    }

    public function isEqual(self $other): bool
    {
        return $this->getName() === $other->getName();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isActive(): bool
    {
        return $this->name === self::ACTIVE;
    }

    public function isBlocked(): bool
    {
        return $this->name === self::ARCHIVED;
    }
}
