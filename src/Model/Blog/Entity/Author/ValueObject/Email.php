<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Author\ValueObject;

use Webmozart\Assert\Assert;

/**
 * Class Email
 * @package App\Model\Blog\Entity\Author\ValueObject
 */
class Email
{
    private $value;

    /**
     * Email constructor.
     * @param string $value
     */
    public function __construct(string $value)
    {
        Assert::notEmpty($value);
        Assert::email($value);
        $this->value = mb_strtolower($value);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function isEqual(self $other): bool
    {
        return $this->getValue() === $other->getValue();
    }
}