<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Author\ValueObject\Type;

use App\Model\Blog\Entity\Author\ValueObject\Email;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

/**
 * Class EmailType
 * @package App\Model\Blog\Entity\Author\ValueObject\Type
 */
class EmailType extends StringType
{
    public const NAME = 'blog_author_email';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof Email ? $value->getValue() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new Email($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) : bool
    {
        return true;
    }
}
