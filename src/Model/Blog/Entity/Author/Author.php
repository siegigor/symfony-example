<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Author;

use App\Model\Blog\Entity\Author\ValueObject\Email;
use App\Model\Blog\Entity\Author\ValueObject\Id;
use App\Model\Blog\Entity\Author\ValueObject\Name;
use App\Model\Blog\Entity\Author\ValueObject\Status;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Author
 * @package App\Model\Blog\Entity\Author
 * @ORM\Entity
 * @ORM\Table(name="blog_authors")
 */
class Author
{
    /**
     * @var Id
     * @ORM\Column(type="blog_author_id")
     * @ORM\Id
     */
    private $id;

    /**
     * @var Name
     * @ORM\Embedded(class="\App\Model\Blog\Entity\Author\ValueObject\Name")
     */
    private $name;

    /**
     * @var Email
     * @ORM\Column(type="blog_author_email")
     */
    private $email;

    /**
     * @var Status
     * @ORM\Column(type="blog_author_status", length=16)
     */
    private $status;

    /**
     * Author constructor.
     * @param Id $id
     * @param Name $name
     * @param Email $email
     */
    public function __construct(Id $id, Name $name, Email $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->status = Status::active();
    }

    public function edit(Name $name, Email $email): void
    {
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Email
     */
    public function getEmail(): Email
    {
        return $this->email;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    public function changeStatus(): void
    {
        $this->status = $this->status->isActive() ? Status::blocked() : Status::active();
    }
}
