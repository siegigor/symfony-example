<?php
declare(strict_types=1);

namespace App\Model\Blog\Entity\Author;

use App\Model\Blog\Entity\Author\ValueObject\Id;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AuthorRepository
 * @package App\Model\Blog\Entity\Author
 */
class AuthorRepository
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * AuthorRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository(Author::class);
        $this->em = $em;
    }

    /**
     * @param Id $id
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function has(Id $id): bool
    {
        return $this->repository->createQueryBuilder('t')
                ->select('COUNT(t.id)')
                ->andWhere('t.id = :id')
                ->setParameter(':id', $id->getValue())
                ->getQuery()->getSingleScalarResult() > 0;
    }


    public function get(Id $id): Author
    {
        /** @var Author $author */
        if (!$author = $this->repository->find($id->getValue())) {
            throw new \DomainException('Author not found.');
        }
        return $author;
    }

    public function add(Author $author): void
    {
        $this->em->persist($author);
    }
}
