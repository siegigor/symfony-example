<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Category\Remove;

use App\Model\Blog\Entity\Category\CategoryRepository;
use App\Model\Blog\Entity\Category\ValueObject\Id;
use App\Model\Flusher;

/**
 * Class Handler
 * @package App\Model\Blog\UseCase\Category\Remove
 */
class Handler
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * Handler constructor.
     * @param CategoryRepository $categoryRepository
     * @param Flusher $flusher
     */
    public function __construct(CategoryRepository $categoryRepository, Flusher $flusher)
    {
        $this->categoryRepository = $categoryRepository;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function handle(Command $command): void
    {
        $category = $this->categoryRepository->get(new Id($command->id));
        if ($this->categoryRepository->hasChild($category->getId())) {
            throw new \DomainException('Category has child categories.');
        }
        $this->categoryRepository->remove($category);
        $this->flusher->flush();
    }
}
