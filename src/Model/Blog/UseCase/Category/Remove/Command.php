<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Category\Remove;

use App\Model\Blog\Entity\Category\Category;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Command
 * @package App\Model\Blog\UseCase\Category\Remove
 */
class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * Command constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->id = $category->getId()->getValue();
    }
}
