<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Category\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Command
 * @package App\Model\Blog\UseCase\Category\Create
 */
class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $title;

    /**
     * @var string
     */
    public $parent;
}
