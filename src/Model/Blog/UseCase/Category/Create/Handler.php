<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Category\Create;

use App\Model\Blog\Entity\Category\Category;
use App\Model\Blog\Entity\Category\CategoryRepository;
use App\Model\Blog\Entity\Category\ValueObject\Id;
use App\Model\Blog\Entity\Category\ValueObject\Title;
use App\Model\Flusher;

/**
 * Class Handler
 * @package App\Model\Blog\UseCase\Category\Create
 */
class Handler
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * Handler constructor.
     * @param CategoryRepository $categoryRepository
     * @param Flusher $flusher
     */
    public function __construct(CategoryRepository $categoryRepository, Flusher $flusher)
    {
        $this->categoryRepository = $categoryRepository;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function handle(Command $command): void
    {
        $category = new Category(
            Id::next(),
            new Title($command->title)
        );
        if ($this->categoryRepository->hasBySlug($category)) {
            throw new \DomainException('Category with this title already exists.');
        }
        if ($command->parent) {
            $parent = $this->categoryRepository->get(new Id($command->parent));
            $category->setAsChildOf($parent);
        }
        $this->categoryRepository->add($category);
        $this->flusher->flush();
    }
}
