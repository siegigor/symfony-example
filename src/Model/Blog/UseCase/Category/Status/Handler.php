<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Category\Status;

use App\Model\Blog\Entity\Category\CategoryRepository;
use App\Model\Blog\Entity\Category\ValueObject\Id;
use App\Model\Blog\Entity\Category\ValueObject\Status;
use App\Model\Flusher;

/**
 * Class Handler
 * @package App\Model\Blog\UseCase\Category\Status
 */
class Handler
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * Handler constructor.
     * @param CategoryRepository $categoryRepository
     * @param Flusher $flusher
     */
    public function __construct(CategoryRepository $categoryRepository, Flusher $flusher)
    {
        $this->categoryRepository = $categoryRepository;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $category = $this->categoryRepository->get(new Id($command->id));
        $category->changeStatus(new Status($command->status));
        $this->flusher->flush();
    }
}
