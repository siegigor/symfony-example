<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Category\Edit;

use App\Model\Blog\Entity\Category\Category;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Command
 * @package App\Model\Blog\UseCase\Category\Edit
 */
class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $title;

    /**
     * @var string|null
     */
    public $parent;

    /**
     * Command constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->id = $category->getId()->getValue();
        $this->title = $category->getTitle()->getTitle();
        $this->parent = $category->getParent() ? $category->getParent()->getId() : null;
    }
}
