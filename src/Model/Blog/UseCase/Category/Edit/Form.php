<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Category\Edit;

use App\ReadModel\Blog\Category\CategoryFetcher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

/**
 * Class Form
 * @package App\Model\Blog\UseCase\Category\Edit
 */
class Form extends AbstractType
{
    /**
     * @var CategoryFetcher
     */
    private $categoryFetcher;

    /**
     * Form constructor.
     * @param CategoryFetcher $categoryFetcher
     */
    public function __construct(CategoryFetcher $categoryFetcher)
    {
        $this->categoryFetcher = $categoryFetcher;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', Type\TextType::class)
            ->add('parent', Type\ChoiceType::class, [
                'choices' => array_flip($this->categoryFetcher->assoc()),
                'expanded' => false,
                'placeholder' => 'Select parent category',
                'required' => false
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Command::class,
        ));
    }
}
