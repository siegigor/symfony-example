<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Author\Status;

use App\Model\Blog\Entity\Author\Author;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Command
 * @package App\Model\Blog\UseCase\Author\Status
 */
class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * Command constructor.
     * @param Author $author
     */
    public function __construct(Author $author)
    {
        $this->id = $author->getId()->getValue();
    }
}