<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Author\Status;

use App\Model\Blog\Entity\Author\AuthorRepository;
use App\Model\Blog\Entity\Author\ValueObject\Id;
use App\Model\Flusher;

/**
 * Class Handler
 * @package App\Model\Blog\UseCase\Author\Status
 */
class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;
    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    /**
     * Handler constructor.
     * @param AuthorRepository $authorRepository
     * @param Flusher $flusher
     */
    public function __construct(AuthorRepository $authorRepository, Flusher $flusher)
    {
        $this->flusher = $flusher;
        $this->authorRepository = $authorRepository;
    }

    public function handle(Command $command): void
    {
        $author = $this->authorRepository->get(new Id($command->id));
        $author->changeStatus();
        $this->flusher->flush();
    }
}
