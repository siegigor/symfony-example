<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Author\Edit;

use App\Model\Blog\Entity\Author\Author;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Command
 * @package App\Model\Blog\UseCase\Author\Edit
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    public $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $firstName;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $lastName;
    /**
     * @var string
     * @Assert\Email()
     */
    public $email;

    /**
     * Command constructor.
     * @param Author $author
     */
    public function __construct(Author $author)
    {
        $this->id = $author->getId()->getValue();
        $this->firstName = $author->getName()->getFirst();
        $this->lastName = $author->getName()->getLast();
        $this->email = $author->getEmail()->getValue();
    }
}
