<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Author\Edit;

use App\Model\Blog\Entity\Author\AuthorRepository;
use App\Model\Blog\Entity\Author\ValueObject\Email;
use App\Model\Blog\Entity\Author\ValueObject\Id;
use App\Model\Blog\Entity\Author\ValueObject\Name;
use App\Model\Flusher;

/**
 * Class Handler
 * @package App\Model\Blog\UseCase\Author\Edit
 */
class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    /**
     * Handler constructor.
     * @param AuthorRepository $authorRepository
     * @param Flusher $flusher
     */
    public function __construct(AuthorRepository $authorRepository, Flusher $flusher)
    {
        $this->flusher = $flusher;
        $this->authorRepository = $authorRepository;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $author = $this->authorRepository->get(new Id($command->id));
        $author->edit(
            new Name(
                $command->firstName,
                $command->lastName
            ),
            new Email($command->email)
        );
        $this->flusher->flush();
    }
}
