<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Author\Create;

use App\Model\Blog\Entity\Author\Author;
use App\Model\Blog\Entity\Author\AuthorRepository;
use App\Model\Blog\Entity\Author\ValueObject\Email;
use App\Model\Blog\Entity\Author\ValueObject\Id;
use App\Model\Blog\Entity\Author\ValueObject\Name;
use App\Model\Flusher;

/**
 * Class Handler
 * @package App\Model\Blog\UseCase\Author\Create
 */
class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    /**
     * Handler constructor.
     * @param AuthorRepository $authorRepository
     * @param Flusher $flusher
     */
    public function __construct(AuthorRepository $authorRepository, Flusher $flusher)
    {
        $this->flusher = $flusher;
        $this->authorRepository = $authorRepository;
    }

    public function handle(Command $command): void
    {
        $id = new Id($command->id);
        if ($this->authorRepository->has($id)) {
            throw new \DomainException('Author already exists.');
        }
        $author = new Author(
            $id,
            new Name(
                $command->firstName,
                $command->lastName
            ),
            new Email($command->email)
        );
        $this->authorRepository->add($author);
        $this->flusher->flush();
    }
}
