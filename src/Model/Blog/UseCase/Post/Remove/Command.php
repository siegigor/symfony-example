<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Post\Remove;

use App\Model\Blog\Entity\Post\Post;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Command
 * @package App\Model\Blog\UseCase\Post\Remove
 */
class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * Command constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->id = $post->getId()->getValue();
    }
}
