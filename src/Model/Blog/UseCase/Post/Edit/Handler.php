<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Post\Edit;

use App\Model\Blog\Entity\Category\CategoryRepository;
use App\Model\Blog\Entity\Category\ValueObject\Id as CategoryId;
use App\Model\Blog\Entity\Post\PostRepository;
use App\Model\Blog\Entity\Post\ValueObject\Id;
use App\Model\Blog\Entity\Post\ValueObject\Title;
use App\Model\Flusher;

/**
 * Class Handler
 * @package App\Model\Blog\UseCase\Post\Edit
 */
class Handler
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * Handler constructor.
     * @param PostRepository $postRepository
     * @param CategoryRepository $categoryRepository
     * @param Flusher $flusher
     */
    public function __construct(
        PostRepository $postRepository,
        CategoryRepository $categoryRepository,
        Flusher $flusher)
    {
        $this->postRepository = $postRepository;
        $this->flusher = $flusher;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param \App\Model\Blog\UseCase\Post\Edit\Command $command
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function handle(Command $command): void
    {
        $post = $this->postRepository->get(new Id($command->id));
        $post->edit(new Title($command->title), $command->content);
        if ($this->postRepository->hasBySlug($post)) {
            throw new \DomainException('Post with this title already exists.');
        }
        $oldCategories = $post->getCategories();
        foreach ($oldCategories as $category) {
            if (!in_array($category->getId()->getValue(), $command->categories)) {
                $post->revokeCategory($category);
            }
        }
        foreach ($command->categories as $id) {
            $category = $this->categoryRepository->get(new CategoryId($id));
            if ($oldCategories->contains($category)) {
                continue;
            }
            $post->assignCategory($category);
        }
        $this->flusher->flush();
    }
}
