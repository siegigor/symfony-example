<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Post\Edit;

use App\Model\Blog\Entity\Category\Category;
use App\Model\Blog\Entity\Post\Post;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Command
 * @package App\Model\Blog\UseCase\Post\Edit
 */
class Command
{
    /**
     * @Assert\NotBlank()
     * @var int
     */
    public $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $title;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $content;

    /**
     * @var array
     * @Assert\NotBlank()
     */
    public $categories;

    /**
     * Command constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->id = $post->getId()->getValue();
        $this->title = $post->getTitle()->getTitle();
        $this->content = $post->getContent();
        $this->categories = $post->getCategories()->map(function (Category $category) {
           return $category->getId()->getValue();
        })->toArray();
    }
}
