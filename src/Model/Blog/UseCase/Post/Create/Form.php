<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Post\Create;

use App\ReadModel\Blog\Category\CategoryFetcher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class Form
 * @package App\Model\Blog\UseCase\Post\Create
 */
class Form  extends AbstractType
{
    /**
     * @var CategoryFetcher
     */
    private $categoryFetcher;

    /**
     * Form constructor.
     * @param CategoryFetcher $categoryFetcher
     */
    public function __construct(CategoryFetcher $categoryFetcher)
    {
        $this->categoryFetcher = $categoryFetcher;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', Type\TextType::class)
            ->add('content', Type\TextareaType::class)
            ->add('categories', Type\ChoiceType::class, [
                'choices' => array_flip($this->categoryFetcher->assoc()),
                'expanded' => true,
                'placeholder' => 'Select categories',
                'required' => true,
                'multiple' => true
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Command::class,
        ));
    }
}
