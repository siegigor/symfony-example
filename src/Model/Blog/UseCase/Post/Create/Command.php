<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Post\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Command
 * @package App\Model\Blog\UseCase\Post\Create
 */
class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $author;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $title;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $content;

    /**
     * @var array
     * @Assert\NotBlank()
     */
    public $categories;

    /**
     * Command constructor.
     * @param string $author
     */
    public function __construct(string $author)
    {
        $this->author = $author;
    }
}
