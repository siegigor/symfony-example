<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Post\Create;

use App\Model\Blog\Entity\Author\AuthorRepository;
use App\Model\Blog\Entity\Author\ValueObject\Id as AuthorId;
use App\Model\Blog\Entity\Category\CategoryRepository;
use App\Model\Blog\Entity\Category\ValueObject\Id as CategoryId;
use App\Model\Blog\Entity\Post\Post;
use App\Model\Blog\Entity\Post\PostRepository;
use App\Model\Blog\Entity\Post\ValueObject\Title;
use App\Model\Flusher;

/**
 * Class Handler
 * @package App\Model\Blog\UseCase\Post\Create
 */
class Handler
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var AuthorRepository
     */
    private $authorRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * Handler constructor.
     * @param PostRepository $postRepository
     * @param AuthorRepository $authorRepository
     * @param CategoryRepository $categoryRepository
     * @param Flusher $flusher
     */
    public function __construct(
        PostRepository $postRepository,
        AuthorRepository $authorRepository,
        CategoryRepository $categoryRepository,
        Flusher $flusher)
    {
        $this->postRepository = $postRepository;
        $this->flusher = $flusher;
        $this->authorRepository = $authorRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param Command $command
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function handle(Command $command): void
    {
        $post = new Post(
            $this->postRepository->nextId(),
            new Title($command->title),
            $this->authorRepository->get(new AuthorId($command->author)),
            $command->content,
            new \DateTimeImmutable()
        );
        if ($this->postRepository->hasBySlug($post)) {
            throw new \DomainException('Post with this title already exists.');
        }
        foreach ($command->categories as $id) {
            $category = $this->categoryRepository->get(new CategoryId($id));
            $post->assignCategory($category);
        }
        $this->postRepository->add($post);
        $this->flusher->flush();
    }
}
