<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Post\Status;

use App\Model\Blog\Entity\Post\PostRepository;
use App\Model\Blog\Entity\Post\ValueObject\Id;
use App\Model\Blog\Entity\Post\ValueObject\Status;
use App\Model\Flusher;

/**
 * Class Handler
 * @package App\Model\Blog\UseCase\Post\Status
 */
class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * Handler constructor.
     * @param PostRepository $postRepository
     * @param Flusher $flusher
     */
    public function __construct(PostRepository $postRepository, Flusher $flusher)
    {
        $this->flusher = $flusher;
        $this->postRepository = $postRepository;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $post = $this->postRepository->get(new Id($command->id));
        $post->changeStatus(new Status($command->status));
        $this->flusher->flush();
    }
}
