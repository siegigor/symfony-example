<?php
declare(strict_types=1);

namespace App\Model\Blog\UseCase\Post\Status;

use App\Model\Blog\Entity\Post\Post;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Command
 * @package App\Model\Blog\UseCase\Post\Status
 */
class Command
{
    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $status;

    /**
     * Command constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->id = $post->getId()->getValue();
        $this->status = $post->getStatus()->getName();
    }
}
